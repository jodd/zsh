# Main user configuration file, will be sourced when starting as a interactive shell.

#[[ -a /home/jan/.readonly_variables ]] && source /home/jan/.readonly_variables
[[ -a /home/jan/.config/zsh/readonly_variables ]] \
  && source /home/jan/.config/zsh/readonly_variables

[[ -a /home/jan/.config/zsh/zsh_shortcut_list ]] \
  && source /home/jan/.config/zsh/zsh_shortcut_list

[[ -a /home/jan/.config/zsh/alias_list ]] \
  && source /home/jan/.config/zsh/alias_list


# [[ -a "$ZDOTDIR"/readonly_variables ]] && source "$ZDOTDIR"/readonly_variables
# [[ -a "$ZDOTDIR"/shortcut_list ]] && source "$ZDOTDIR"/shortcut_list
# [[ -a "$ZDOTDIR"/alias_list ]] && source "$ZDOTDIR"/alias_list
# [ -z "$ZDOTDIR/LS_colors" ] && source "$ZDOTDIR/LS_colors"

#fzf
[[ -a /usr/share/fzf/key-bindings.zsh ]] && source /usr/share/fzf/key-bindings.zsh
[[ -a /usr/share/fzf/completion.zsh ]] && source /usr/share/fzf/completion.zsh

# https://github.com/joshtronic
# tiny git check, if it's dirty, then tell!
git_prompt() {
  BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/*\(.*\)/\1/')

  if [[ ! -z $BRANCH ]]; then
    echo -n "%F{yellow}$BRANCH"

    if [[ ! -z "$(git status --short)" ]]; then
      echo " %F{red}✗"
    fi
  fi
}


PS1='%F{blue}%d$(git_prompt)%F{244}%F{reset}%$
%n@%m '


# I prefere closing window with super + q
# this will remove closing zsh with CTRL+D
setopt IGNORE_EOF


setopt promptsubst
autoload -U colors && colors


# History moved out of cache directory:
HISTSIZE=10000
SAVEHIST=10000
# HISTFILE=$XDG_CACHE_HOME/zsh/history
HISTFILE=/home/jan/.history


setopt appendhistory autocd extendedglob nomatch notify completealiases
unsetopt beep
zstyle :compinstall filename '/home/jan/.zshrc'


# Basic auto/tab complete:
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.


export KEYTIMEOUT=5


# Use vim keys in tab complete menu:
bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

bindkey -s '^o' 'lfcd\n'


# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line


# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}


zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}


zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.


# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}

#TODO
#need somewhere to:
# setxkbmap -option ctrl:nocaps
# setxkbmap -model pc105 -layout no -option caps:none

# The ttyctl command can be used to "freeze/unfreeze" the terminal.
# Many programs change the terminal state, and often do not restore
# terminal settings on exiting abnormally. To avoid the need to
# manually reset the terminal, use the following:
ttyctl -f


# vi mode
bindkey -v
# Load zsh-syntax-highlighting; should be last.
[ -z /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null



# 1. 	/etc/zsh/zshenv
# 2.	~/.zshenv
# 3.	/etc/zsh/zprofile
# 4.	/etc/profile
# 5.	~/.zprofile
# 6.	/etc/zsh/zshrc
# 7.	~/.zshrc			THIS FILE
# 8.	/etc/zsh/zlogin
# 9.	~/.zlogin
# 10.	/etc/zsh/zlogout
# 11.	~/.zlogout

