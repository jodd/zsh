# This file is generally used for automatic execution of user's scripts at login.


# 1. 	/etc/zsh/zshenv
# 2.	~/.zshenv
# 3.	/etc/zsh/zprofile
# 4.	/etc/profile
# 5.	~/.zprofile 		THIS FILE
# 6.	/etc/zsh/zshrc
# 7.	~/.zshrc
# 8.	/etc/zsh/zlogin
# 9.	~/.zlogin
# 10.	/etc/zsh/zlogout
# 11.	~/.zlogout

## https://www.youtube.com/watch?v=9bO7hYT2psc
## Broadie use zprofile to:
## if [[ "$(tty)" = "/dev/tty1" ]]; then
#   pgrep bspwm || startx "$XDG_CONFIG_HOME/X11/xinitrc"
#   fi
#   eval "$(gh comletion -s zsh)"
## 

