# Default directory is ~/
# This is changed to tidy up, all files is moved to new ZDOTDIR.
# a symlink is left in ~/ to .config/zsh/zshenv

# export ZDOTDIR="/home/jan/.config/zsh"
# export ZDOTDIR="/home/jan/"

typeset -U PATH path
path=(~/.local/bin $path[@])
export PATH



##
# 1. 	/etc/zsh/zshenv
# 2.	~/.zshenv			THIS FILE
# 3.	/etc/zsh/zprofile
# 4.	/etc/profile
# 5.	~/.zprofile
# 6.	/etc/zsh/zshrc
# 7.	~/.zshrc
# 8.	/etc/zsh/zlogin
# 9.	~/.zlogin
# 10.	/etc/zsh/zlogout
# 11.	~/.zlogout

