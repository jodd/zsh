#
# ~/.bashrc
#


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
# PS1='[\u@\h \W]\$ '

[[ "$(whoami)" = "root" ]] && return

export HISTCONTROL=ignoreboth:erasedups

#ignore upper and lowercase when TAB completion
bind "set completion-ignore-case on"

#shopt
shopt -s autocd # change to named directory
shopt -s cdspell # autocorrects cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s expand_aliases # expand aliases

#[[ -f ~/.config/zsh/alias_list ]] && . ~/.config/zsh/alias_list
#[[ -f ~/.config/zsh/readonly_variables ]] && . ~/.config/zsh/readonly_variables
#[[ -f ~/.config/zsh/zsh_shortcut_list ]] && . ~/.config/zsh/zsh_shortcut_list
# [[ -f ~/.bashrc-personal ]] && . ~/.bashrc-personal


# bind '"\e[A":history-search-backward'
# bind '"\e[A":history-search-forward'

# if [ -d "$HOME/.local/qbin" ] ;
#   then PATH="$HOME/.local/qbin:$PATH"
# fi

# if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
#     exec startx /home/jan/.xinitrc -- /home/jan/.xserverrc vt1  
    # exec startx "$XDG_CONFIG_HOME/X11/xinitrc" -- "$XDG_CONFIG_HOME/X11/xserverrc" vt1
# exec startx
# startx
# fi
